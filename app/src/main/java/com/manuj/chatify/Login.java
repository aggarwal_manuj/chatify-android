package com.manuj.chatify;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class Login extends AppCompatActivity {

    EditText editTextMobile;
    Button buttoncon;
    FirebaseUser firebaseUser;

    FirebaseFirestore db;
    String uid;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == 111) {
                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);

                finish();
//            }else if (msg.what == 112) {
//                Intent intent = new Intent(SplashActivity.this, Verification1.class);
//                startActivity(intent);
//
//                finish();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextMobile = findViewById(R.id.editTextPhone);
        buttoncon = (Button) findViewById(R.id.buttonContinue);


        buttoncon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = editTextMobile.getText().toString().trim();
                String phone = "+91" + mobile;
                if (mobile.isEmpty() && mobile.length() != 10) {
                    editTextMobile.setError("Enter a valid mobile");
                    editTextMobile.setFocusable(true);
                    editTextMobile.setText("");
                    return;
                } else {

                    Intent intent = new Intent(Login.this, Verify_PhoneAuth.class);
                    intent.putExtra("phonenumber", phone);
                    startActivity(intent);
                    finish();
                }
            }
        });


        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null) {
            uid = firebaseUser.getUid();
            db = FirebaseFirestore.getInstance();
            handler.sendEmptyMessageDelayed(111, 500);

        }
    }
}