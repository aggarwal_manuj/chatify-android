package com.manuj.chatify.Fragments;

import com.manuj.chatify.Notifications.MyResponse;
import com.manuj.chatify.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAFHlUBU8:APA91bHTwUIGJX7VF1SLUV4dJl4Hdk6oMP9roTGh3_OL9rqgzx5v5MEyS3noHvovLFmF9CN2gQfKj2Rl8L978RzyjPKBMoq9KcCFYFb0S9TobrFUmsuyGlA8vvt9qofh1_l6-GGrZUwG"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
