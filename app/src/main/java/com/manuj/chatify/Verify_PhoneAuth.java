package com.manuj.chatify;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.manuj.chatify.Model.User;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Verify_PhoneAuth extends AppCompatActivity {

    private String verificationID;
    private FirebaseAuth mAuth;
    PinView pinView;
    public TextView Resendotp, Timing;
    String phonenumber;
    DatabaseReference reference;
    User user;
    public int counter=30;
    FirebaseUser firebaseUser;
    FirebaseFirestore db;

    boolean PhoneVerify;

    PhoneAuthProvider.ForceResendingToken token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify__phone_auth);


        mAuth = FirebaseAuth.getInstance();
        pinView = findViewById(R.id.editTextCode);
        Resendotp = findViewById(R.id.resend);
        Timing = findViewById(R.id.timer);
        db = FirebaseFirestore.getInstance();




//        progressBar = findViewById(R.id.progressbar);
        phonenumber = getIntent().getStringExtra("phonenumber");
        user = new User();

        sendVerificationCode(phonenumber);

        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                Timing.setText("00:"+String.format("%02d", counter));
                counter--;
            }

            public void onFinish() {
                Resendotp.setVisibility(View.VISIBLE);
                Timing.setVisibility(View.GONE);
            }
        }.start();


        Resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                progressBar.setVisibility(View.VISIBLE);
                resendVerificationCode(phonenumber,token);
//                progressBar.setVisibility(View.INVISIBLE);
            }
        });

        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code=pinView.getText().toString().trim();

//                if(PhoneVerify==false) {
//                    pinView.setText("");
//                    pinView.setFocusable(true);
//                    pinView.setError("Enter correct Otp");
//                    return;
//                }
                verifyVerificationCode(code);



            }
        });
    }

    void saveUserInFirebase() {

        FirebaseUser firebaseUser = mAuth.getCurrentUser();
        assert firebaseUser != null;
        String userid = firebaseUser.getUid();

        reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", userid);
        hashMap.put("username", phonenumber);
        hashMap.put("imageURL", "default");
        hashMap.put("status", "offline");
        hashMap.put("bio", "");
        hashMap.put("search",phonenumber);
        hashMap.put("IMEI",getIMEI());

        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(Verify_PhoneAuth.this,"Registered Successfully",Toast.LENGTH_LONG);
                    finish();
                }
                else {
                        Toast.makeText(Verify_PhoneAuth.this, "Something Went Wrong !!", Toast.LENGTH_LONG).show();
                        }
            }
        });

//        user.mobile=phonenumber;
//        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        String uid = firebaseUser.getUid();
//
//
//        FirebaseFirestore db = FirebaseFirestore.getInstance();
//        db.collection("Users").document(uid).set(user)
//                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if (task.isComplete()) {
//                            Toast.makeText(Verify_PhoneAuth.this,"Registered Successfully",Toast.LENGTH_LONG);
//
//                        } else {
//                            Toast.makeText(Verify_PhoneAuth.this, "Something Went Wrong !!", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });

    }


    private void verifyVerificationCode(String otp) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationID, otp);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }


    public void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(Verify_PhoneAuth.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
//                            PhoneVerify=true;
                            Intent intent=new Intent(Verify_PhoneAuth.this,MainActivity.class);
                            startActivity(intent);
                            saveUserInFirebase();
                            finish();
                            Toast.makeText(Verify_PhoneAuth.this,"Login Success!",Toast.LENGTH_SHORT);


                        }
                        else {
                            String message = "Something is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.Verifyotp), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
//                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });

    }

//    private void goToNextActivity() {
//        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
//        if (firebaseUser == null) {
//            Intent intent = new Intent(Verify_PhoneAuth.this, MainActivity.class);
//            intent.putExtra("phone", phonenumber);
//            startActivity(intent);
//            finish();
//        }
  //  }
    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mobile,
                30,
                TimeUnit.SECONDS,
                Verify_PhoneAuth.this,
                mCallbacks);
    }

    private   void resendVerificationCode(String phoneNumber,
                                          PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                30,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }



    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();


            if (code != null) {
//                progressBar.setVisibility(View.VISIBLE);
                pinView.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            Toast.makeText(Verify_PhoneAuth.this, e.getMessage(), Toast.LENGTH_LONG).show();

        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationID = s;
            token=forceResendingToken;

        }

    };
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent =new Intent(Verify_PhoneAuth.this, Login.class);
        startActivity(intent);
        finish();

    }

    public String getIMEI( ) {

        String deviceId;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
        {
            deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (this.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return "";
                }
            }
            assert mTelephony != null;
            if (mTelephony.getDeviceId() != null)
            {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                {
                    deviceId = mTelephony.getImei();
                }else {
                    deviceId = mTelephony.getDeviceId();
                }
            } else {
                deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
            }
        }
        Log.d("deviceId", deviceId);
        return deviceId;
    }

    }